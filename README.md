# Before you begin.
Please ensure you have docker and docker-compose installed.


# Setup volumes
In docker-compose.yml, configure the volumes options for each service.

# Getting started

```
docker-compose up
```

## Urls


| *Tool* | *Link* | *Credentials* |
| ------------- | ------------- | ------------- |
| Jenkins | http://${docker-machine ip default}:18080/ | no login required |
| SonarQube | http://${docker-machine ip default}:19000/ | admin/admin |
| Artifactory | http://${docker-machine ip default}:8081/artifactory | 

## Jenkins Security
Please ensure to enable security in Jenkins.


